# Just my personal graphql api

[![Netlify Status](https://api.netlify.com/api/v1/badges/7e104dda-1939-4204-8c78-15745f2461b2/deploy-status)](https://app.netlify.com/sites/chrisbarry-personal-api/deploys)

A pretty basic, but easily editable personal api. Just needs a few env variables for fediverse apps, a ghost photoblog, and lastfm access tokens

You'll need to install `netlify-cli` to run locally for dev
