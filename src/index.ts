import schema from "./db/graphql";

import { ApolloServer } from "@apollo/server";
import {
  ApolloServerPluginLandingPageLocalDefault,
  ApolloServerPluginLandingPageProductionDefault,
} from "@apollo/server/plugin/landingPage/default";
import { startStandaloneServer } from "@apollo/server/standalone";

const server = new ApolloServer({
  schema,
  introspection: true,
  csrfPrevention: false,
  plugins: [
    process.env.NODE_ENV !== "production"
      ? ApolloServerPluginLandingPageLocalDefault()
      : ApolloServerPluginLandingPageProductionDefault({ footer: false }),
  ],
});

startStandaloneServer(server, {
  listen: { host: "0.0.0.0", port: +(process.env.PORT ?? 3000) },
}).then(({ url }) => console.info(`Listening on ${url}`));
