import { GraphQLID, GraphQLObjectType, GraphQLString } from "graphql/type";

const photo = new GraphQLObjectType({
  name: "Photo",
  description: "A Photo from my blog",
  fields: () => ({
    title: { type: GraphQLString, description: "The Photo Title" },
    excerpt: { type: GraphQLString, description: "An excerpt of a post" },
    photo: { type: GraphQLString, description: "The photo source" },
    url: { type: GraphQLString, description: "a link to the post" },
    id: { type: GraphQLID, description: "A unique identifier for a photo" },
  }),
});

export default photo;
