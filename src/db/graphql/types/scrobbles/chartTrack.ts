import { GraphQLInt, GraphQLObjectType } from "graphql/type";
import malojaTrack from "./malojaTrack";

const chartTrack = new GraphQLObjectType({
  name: "chartTrack",
  description: "An item on most played chart",
  fields: () => ({
    track: { type: malojaTrack, description: "Information about the track" },
    scrobbles: {
      type: GraphQLInt,
      description:
        "How many times the item has been played in the given time period",
    },
    rank: {
      type: GraphQLInt,
      description: "The item's rank",
    },
    id: {
      type: GraphQLInt,
      description: "The item's uid",
      resolve: ({ track_id }: { track_id: number }) => track_id,
    },
  }),
});
export default chartTrack;
