import { format, sub } from "date-fns";
import { GraphQLList, GraphQLObjectType, GraphQLString } from "graphql/type";
import limit from "../../args/limit";
import period from "../../args/period";
import chartAlbum from "./chartAlbum";
import chartArtist from "./chartArtist";
import chartTrack from "./chartTrack";
import type {
  MalojaChartAlbum,
  MalojaChartArtist,
  MalojaChartTrack,
  MalojaResponse,
  MalojaScrobble,
} from "./datamodel";
import { Period } from "./datamodel";
import scrobble from "./scrobble";

const composeDateRange = (url: URL, range: Period): URL => {
  switch (range) {
    case Period.WEEK: {
      url.searchParams.append(
        "since",
        format(sub(new Date(), { days: 7 }), "yyyy/MM/dd"),
      );
      return url;
    }
    case Period.MONTH: {
      url.searchParams.append(
        "since",
        format(sub(new Date(), { days: 30 }), "yyyy/MM/dd"),
      );
      return url;
    }
    case Period.THREE_MONTH: {
      url.searchParams.append(
        "since",
        format(sub(new Date(), { days: 90 }), "yyyy/MM/dd"),
      );
      return url;
    }
    case Period.SIX_MONTH: {
      url.searchParams.append(
        "since",
        format(sub(new Date(), { days: 180 }), "yyyy/MM/dd"),
      );
      return url;
    }
    case Period.YEAR: {
      url.searchParams.append(
        "since",
        format(sub(new Date(), { days: 365 }), "yyyy/MM/dd"),
      );
      return url;
    }
    default:
      return url;
  }
};

async function queryMalojaApi(
  path: "scrobbles",
  timePeriod: Period,
  max: number,
): Promise<MalojaScrobble[]>;
async function queryMalojaApi(
  path: "charts/artists",
  timePeriod: Period,
  max: number,
): Promise<MalojaChartArtist[]>;
async function queryMalojaApi(
  path: "charts/albums",
  timePeriod: Period,
  max: number,
): Promise<MalojaChartAlbum[]>;
async function queryMalojaApi(
  path: "charts/tracks",
  timePeriod: Period,
  max: number,
): Promise<MalojaChartTrack[]>;
async function queryMalojaApi(
  path: "scrobbles" | `charts/${"albums" | "artists" | "tracks"}`,
  timePeriod: Period,
  max: number,
): Promise<unknown[]> {
  const queryUrl = composeDateRange(
    new URL(path, process.env.SCROBBLE_API_ENDPOINT),
    timePeriod,
  );
  queryUrl.searchParams.append("perpage", max.toString());
  try {
    const response = await fetch(queryUrl);
    if (response.ok) {
      const json = (await response.json()) as MalojaResponse;
      return json.list.slice(0, max);
    }
    throw new Error(response.statusText);
  } catch {
    return [];
  }
}

const getRecentTracks = async (timePeriod: Period, max = 10) => {
  const tracks = await queryMalojaApi("scrobbles", timePeriod, max);
  return tracks;
};
const getTopAlbums = async (timePeriod: Period, max = 10) => {
  const tracks = await queryMalojaApi("charts/albums", timePeriod, max);
  return tracks;
};
const getTopArtists = async (timePeriod: Period, max = 10) => {
  const tracks = await queryMalojaApi("charts/artists", timePeriod, max);
  return tracks;
};
const getTopTracks = async (timePeriod: Period, max = 10) => {
  const tracks = await queryMalojaApi("charts/tracks", timePeriod, max);
  return tracks;
};

const scrobbles = new GraphQLObjectType({
  name: "Scrobbles",
  description: "My Music listening",
  fields: () => ({
    mostPlayedTracks: {
      args: { limit, period },
      type: new GraphQLList(chartTrack),
      description: "My most played songs",
      resolve: async (_, { limit: max = 10, period: timePeriod }) =>
        getTopTracks(timePeriod as Period, max as number),
    },
    mostPlayedArtists: {
      args: { limit, period },
      type: new GraphQLList(chartArtist),
      description: "My most played artists",
      resolve: async (_, { limit: max = 10, period: timePeriod }) =>
        getTopArtists(timePeriod as Period, max as number),
    },
    mostPlayedAlbums: {
      args: { limit, period },
      type: new GraphQLList(chartAlbum),
      description: "My most played albums",
      resolve: async (_, { limit: max = 10, period: timePeriod }) =>
        getTopAlbums(timePeriod as Period, max as number),
    },
    recentSongs: {
      args: { limit },
      type: new GraphQLList(scrobble),
      description: "A Song I listened to",
      resolve: async (_, { limit: max = 5 }) =>
        getRecentTracks(Period.OVERALL, max as number),
    },
    url: {
      type: GraphQLString,
      description: "My scrobble tracking url",
      resolve: ({ url }: { url: string }) => url,
    },
  }),
});

export default scrobbles;
