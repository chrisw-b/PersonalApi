import { GraphQLInt, GraphQLObjectType } from "graphql/type";
import malojaTrack from "./malojaTrack";

const scrobble = new GraphQLObjectType({
  name: "scrobble",
  description: "An item I listened to",
  fields: () => ({
    time: {
      type: GraphQLInt,
      description: "The time it was played",
    },
    track: {
      type: malojaTrack,
      description: "Information about the track",
    },
  }),
});
export default scrobble;
