import {
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";

const chartArtist = new GraphQLObjectType({
  name: "chartArtist",
  description: "An item on most played chart",
  fields: () => ({
    scrobbles: {
      type: GraphQLInt,
      description:
        "How many times the item has been played in the given time period",
    },
    real_scrobbles: {
      type: GraphQLInt,
      description: "Scrobbles Maloja defines as 'really' by this artist",
    },
    artist: {
      type: GraphQLString,
      description: "Name of the artist",
    },
    associated_artists: {
      type: new GraphQLList(GraphQLString),
      description: "Name of the artist",
    },
    rank: {
      type: GraphQLInt,
      description: "The item's rank",
    },
    id: {
      type: GraphQLInt,
      description: "The item's uid",
      resolve: ({ artist_id }: { artist_id: number }) => artist_id,
    },
  }),
});
export default chartArtist;
