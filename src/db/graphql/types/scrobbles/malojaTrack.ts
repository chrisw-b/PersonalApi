import {
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";
import malojaAlbum from "./malojaAlbum";

const malojaTrack = new GraphQLObjectType({
  name: "malojaTrack",
  description: "An item on most played chart",
  fields: () => ({
    artists: {
      type: new GraphQLList(GraphQLString),
      description: "artists on the track",
    },
    title: {
      type: GraphQLString,
      description: "Name of the track",
    },
    album: {
      type: malojaAlbum,
      description: "Album info for the track",
    },
    length: {
      type: GraphQLInt,
      description: "The track length",
    },
  }),
});
export default malojaTrack;
