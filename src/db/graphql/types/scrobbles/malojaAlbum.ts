import { GraphQLList, GraphQLObjectType, GraphQLString } from "graphql/type";

const malojaAlbum = new GraphQLObjectType({
  name: "malojaAlbum",
  description: "An item on most played chart",
  fields: () => ({
    artists: {
      type: new GraphQLList(GraphQLString),
      description: "artists on the album",
    },
    albumtitle: {
      type: GraphQLString,
      description: "Name of the album",
    },
  }),
});
export default malojaAlbum;
