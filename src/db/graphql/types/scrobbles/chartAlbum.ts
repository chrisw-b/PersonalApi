import { GraphQLInt, GraphQLObjectType } from "graphql/type";

import malojaAlbum from "./malojaAlbum";

const chartAlbum = new GraphQLObjectType({
  name: "chartAlbum",
  description: "An item on most played chart",
  fields: () => ({
    scrobbles: {
      type: GraphQLInt,
      description:
        "How many times the item has been played in the given time period",
    },
    album: {
      type: malojaAlbum,
      description: "Information about the album",
    },
    rank: {
      type: GraphQLInt,
      description: "The item's rank",
    },
    id: {
      type: GraphQLInt,
      description: "The item's uid",
      resolve: ({ album_id }: { album_id: number }) => album_id,
    },
  }),
});
export default chartAlbum;
