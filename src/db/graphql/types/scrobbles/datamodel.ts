export type MalojaAlbum = {
  artists: string[];
  albumtitle: string;
};

export type MalojaTrack = {
  artists: string[];
  title: string;
  album: MalojaAlbum;
  length: string | null;
};

export type MalojaScrobble = {
  time: number;
  track: MalojaTrack;
  duration: null | number;
  origin: string;
};

export type MalojaChartTrack = {
  scrobbles: number;
  track_id: number;
  rank: number;
  track: MalojaTrack;
};

export type MalojaChartAlbum = {
  scrobbles: number;
  album_id: number;
  rank: number;
  album: MalojaAlbum;
};

export type MalojaChartArtist = {
  scrobbles: number;
  real_scrobbles: number;
  artist_id: number;
  artist: string;
  rank: number;
  associated_artists: string[];
};

export type MalojaResponse<T = unknown> = {
  status: string;
  list: T[];
  pagination?: {
    page: number;
    perpage: number | null;
    next_page: string | null;
    prev_page: string | null;
  };
};

export enum Period {
  OVERALL = "overall",
  WEEK = "7day",
  MONTH = "1month",
  THREE_MONTH = "3month",
  SIX_MONTH = "6month",
  YEAR = "12month",
}
