import {
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";
import art from "./art";

const chartItem = new GraphQLObjectType({
  name: "chartItem",
  description: "An item on most played chart",
  fields: () => ({
    name: {
      type: GraphQLString,
      description:
        "The item name, dependent on what kind of query (either album or song)",
    },
    artist: {
      type: GraphQLString,
      description: "The artist for the song or album",
    },
    playcount: {
      type: GraphQLInt,
      description:
        "How many times the item has been played in the given time period",
    },
    mbid: {
      type: GraphQLString,
      description:
        "The item's unique musicbrains id, useful for finding it on last.fm",
    },
    art: {
      type: new GraphQLList(art),
      description: "A list of art for the artist or album",
    },
  }),
});
export default chartItem;
