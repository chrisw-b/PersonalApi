import {
  GraphQLEnumType,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";
import { LastFmImageSize } from "lastfm-njs";

const art = new GraphQLObjectType({
  name: "art",
  description: "An image provided by last.fm",
  fields: () => ({
    url: {
      type: GraphQLString,
      description: "The file location",
    },
    size: {
      type: new GraphQLEnumType({
        name: "size",
        values: {
          SMALL: { value: LastFmImageSize.SMALL },
          MEDIUM: { value: LastFmImageSize.MEDIUM },
          LARGE: { value: LastFmImageSize.LARGE },
          EXTRALARGE: { value: LastFmImageSize.EXTRALARGE },
          MEGA: { value: LastFmImageSize.MEGA },
        },
      }),
      description: "The image size",
    },
  }),
});
export default art;
