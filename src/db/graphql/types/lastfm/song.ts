import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";
import art from "../../types/lastfm/art";

const song = new GraphQLObjectType({
  name: "Song",
  description: "A Song I've Listened To",
  fields: () => ({
    title: { type: GraphQLString, description: "The Song Name" },
    nowplaying: {
      type: GraphQLBoolean,
      description: "Whether the song is currently playing",
    },
    artist: { type: GraphQLString, description: "The Artist Name" },
    art: {
      type: new GraphQLList(art),
      description: "A list of art for the artist or album, in url form",
    },
    id: { type: GraphQLID, description: "A unique ID" },
    mbid: {
      type: GraphQLString,
      description:
        "The item's unique musicbrains id, useful for finding it on last.fm",
    },
  }),
});
export default song;
