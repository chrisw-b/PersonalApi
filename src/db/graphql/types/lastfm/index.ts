import crypto from "node:crypto";

import { GraphQLList, GraphQLObjectType, GraphQLString } from "graphql/type";
import LastFM from "lastfm-njs";
import type { LastFmImageSize, Period } from "lastfm-njs";
import limit from "../../args/limit";
import period from "../../args/period";
import chartItem from "./chartItem";
import song from "./song";

type LastFMTrack = {
  title: string;
  artist: string;
  nowplaying: "true" | "false" | boolean;
  art: { url: string; size: LastFmImageSize }[];
};

let client: LastFM | undefined;

// love to singleton
const getLastFmClient = (): LastFM => {
  if (!client) {
    client = new LastFM({
      apiKey: process.env.LASTFM_KEY,
      apiSecret: process.env.LASTFM_SECRET,
    });
  }

  return client;
};

const getLastFmSongs = async (max: number): Promise<LastFMTrack[]> => {
  const lastFm = getLastFmClient();
  const maxSongs = max > 50 ? 50 : max;

  const tracksReq = await lastFm.user_getRecentTracks({
    user: process.env.LASTFM_ID,
    limit: maxSongs,
  });

  if (!tracksReq.success) {
    throw new Error(tracksReq.error.toString());
  }

  let tracks = tracksReq.track;
  if (max !== tracks.length) {
    tracks = tracks.slice(0, max); // sometimes last.fm returns 2 tracks when you ask for 1
  }
  return tracks.map((track) => ({
    title: track.name,
    artist: track.artist["#text"],
    mbid: track.mbid,
    nowplaying: false,
    art: track.image.map((art) => ({ url: art["#text"], size: art.size })),
    id: crypto
      .createHash("sha1")
      .update(track.name + track.artist["#text"])
      .digest("base64"),
  }));
};

const getTopTracks = async (timePeriod: Period, max: number) => {
  const lastFm = getLastFmClient();
  const tracksReq = await lastFm.user_getTopTracks({
    user: process.env.LASTFM_ID,
    limit: max,
    period: timePeriod,
  });

  if (!tracksReq.success) {
    throw new Error(tracksReq.error.toString());
  }

  const tracks = tracksReq.track;
  return tracks.map(({ name, artist, playcount, image, mbid }) => ({
    name,
    artist: artist.name,
    playcount,
    art: image.map((art) => ({ url: art["#text"], size: art.size })),
    mbid,
    id: crypto
      .createHash("sha1")
      .update(name + artist.name)
      .digest("base64"),
  }));
};

const getTopArtists = async (timePeriod: Period, max: number) => {
  const lastFm = getLastFmClient();

  const artistsReq = await lastFm.user_getTopArtists({
    user: process.env.LASTFM_ID,
    limit: max,
    period: timePeriod,
  });

  if (!artistsReq.success) {
    throw new Error(artistsReq.error.toString());
  }
  const artists = artistsReq.artist;

  return artists.map(({ name, playcount, image, mbid }) => ({
    artist: name,
    playcount,
    mbid,
    art: image.map((art) => ({ url: art["#text"], size: art.size })),
    id: crypto.createHash("sha1").update(name).digest("base64"),
  }));
};

const getTopAlbums = async (timePeriod: Period, max: number) => {
  const lastFm = getLastFmClient();

  const albumsReq = await lastFm.user_getTopAlbums({
    user: process.env.LASTFM_ID,
    limit: max,
    period: timePeriod,
  });

  if (!albumsReq.success) {
    throw new Error(albumsReq.error.toString());
  }
  const albums = albumsReq.album;

  return albums.map(({ name, artist, playcount, image }) => ({
    name,
    artist: artist.name,
    playcount,
    art: image.map((art) => ({ url: art["#text"], size: art.size })),
    id: crypto
      .createHash("sha1")
      .update(name + artist.name)
      .digest("base64"),
  }));
};
const lastFM = new GraphQLObjectType({
  name: "LastFM",
  description: "My Github Info",
  fields: () => ({
    mostPlayedTracks: {
      args: { limit, period },
      type: new GraphQLList(chartItem),
      description: "My most played songs",
      resolve: async (_, { limit: max = 10, period: timePeriod }) =>
        getTopTracks(timePeriod as Period, max as number),
    },
    mostPlayedArtists: {
      args: { limit, period },
      type: new GraphQLList(chartItem),
      description: "My most played artists",
      resolve: async (_, { limit: max = 10, period: timePeriod }) =>
        getTopArtists(timePeriod as Period, max as number),
    },
    mostPlayedAlbums: {
      args: { limit, period },
      type: new GraphQLList(chartItem),
      description: "My most played albums",
      resolve: async (_, { limit: max = 10, period: timePeriod }) =>
        getTopAlbums(timePeriod as Period, max as number),
    },
    recentSongs: {
      args: { limit },
      type: new GraphQLList(song),
      description: "A Song I listened to",
      resolve: async (_, { limit: max = 5 }) => getLastFmSongs(max as number),
    },
    nowplaying: {
      type: song,
      description: "What's playing right now",
      resolve: async () => {
        const songs = await getLastFmSongs(1);
        const mostRecentSong = songs.length > 0 && songs[0];
        const nowPlaying =
          mostRecentSong &&
          (mostRecentSong.nowplaying === "true" ||
            (typeof mostRecentSong.nowplaying === "boolean" &&
              mostRecentSong.nowplaying));

        return nowPlaying
          ? { ...mostRecentSong, nowplaying: true }
          : {
              ...(mostRecentSong ?? { title: undefined, artist: undefined }),
              nowplaying: false,
            };
      },
    },
    url: {
      type: GraphQLString,
      description: "My Last.FM url",
      resolve: ({ url }: { url: string }) => url,
    },
  }),
});

export default lastFM;
