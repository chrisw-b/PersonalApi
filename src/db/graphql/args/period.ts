import { GraphQLEnumType } from "graphql/type";
import { Period } from "lastfm-njs";

const period = {
  name: "period",
  description: "The time period",
  type: new GraphQLEnumType({
    name: "period",
    values: {
      overall: { value: Period.OVERALL },
      week: { value: Period.WEEK },
      month: { value: Period.MONTH },
      threeMonth: { value: Period.THREE_MONTH },
      sixMonth: { value: Period.SIX_MONTH },
      year: { value: Period.YEAR },
    },
  }),
};

export default period;
